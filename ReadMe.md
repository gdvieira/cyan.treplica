﻿## Project

This page has examples for the use of Cyan metaobjects with treplica. Note that the example that are in the article itself are not here. Currently, the Cyan compiler only works on Windows (sorry about that).

In order to run the example, [download and uncompress file lib.zip](http://cyan-lang.org/lib/) from the Cyan site, www.cyan-lang.org. Download the project from http://cyan-lang.org/projeto/. 

Suppose lib.zip and projeto.zip were uncompressed into directories

			D:\treplica\lib
			D:\treplica\projeto
This last directory has subdirectories

			app
			cyanMetaobjects
				meta
					treplica				
			jar
			lib

In which `treplica` is subdirectory of `meta` that is subdirectory of `cyanMetaobjects`. The Java code of the metaobjects `treplicaInit`, `treplicaAction`,  `loadNonDeterministicFiles`, and `nonDeterministic` are inside directory `treplica`. To change these metaobjects or to compile them, read the [primer on how to build metaobjects](http://cyan-lang.org/first/).

To test the examples, open a DOS command prompt and make `D:\` the current drive. Create a directory `D:\treplica\tmpDirs` and subdirectories

		D:\treplica\tmpDirs\magic1
		D:\treplica\tmpDirs\magic2

Go to directory  `D:\treplica\projeto\app`

Now there are four examples, calculator, rogue, treplica, verify.  To call them, copy each the following commands to the DOS prompt:
1.       start /wait call-exem calculator "D:\treplica\projeto" "D:\treplica\lib"   "\treplica\tmpDirs"
1.       start /wait call-exem rogue "D:\treplica\projeto" "D:\treplica\lib"   "\treplica\tmpDirs"
1.       start /wait call-exem treplica "D:\treplica\projeto" "D:\treplica\lib"   "\treplica\tmpDirs"
1.       start /wait call-exem verify  "D:\treplica\projeto" "D:\treplica\lib"   "\treplica\tmpDirs"

Do not put the drive letter in the last directory. That is, do not use this: `D:\treplica\tmpDirs`. 

