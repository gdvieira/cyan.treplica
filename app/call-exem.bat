@echo off
REM exemplo of how to call:
REM thisFile calculator "\Dropbox\research\projeto" "\Dropbox\Cyan\lib" "\Dropbox\19-2\doc\treplica\treplicaDirs"

if "%~1"=="" goto Noargument
if "%~2"=="" goto Noargument
if "%~3"=="" goto Noargument
if "%~4"=="" goto Noargument

SET exname=%1
SET projeto=%2
SET lib=%3
SET magic=%4


REM SET exname="calculator"
REM SET projeto="\Dropbox\research\projeto"
REM SET lib="\Dropbox\Cyan\lib"
REM SET magic="\Dropbox\19-2\doc\treplica\treplicaDirs"


SET exname_nq=%exname:"=%
SET projeto_nq=%projeto:"=%
SET lib_nq=%lib:"=%
SET magic_nq=%magic:"=%


SET CYAN_HOME=%lib_nq%

REM echo %exname_nq%
REM echo %projeto_nq%
REM echo %lib_nq%
REM echo %magic_nq%



del "%magic_nq%\magic1\*.*" /f /s /q
del "%magic_nq%\magic2\*.*" /f /s /q

SET arg_0_Process_1="%magic_nq%\magic1"
SET arg_0_Process_2="%magic_nq%\magic2"
SET arg_1_Process_1=""
SET arg_1_Process_2=""

SET validFirstArgument=false

if %exname_nq%==calculator  ( 
	 SET validFirstArgument=true 
 	 )


if %exname_nq%==rogue  ( 
     SET arg_1_Process_1="1"
	 SET arg_1_Process_2="2"
	 SET validFirstArgument=true 
	 )
if %exname_nq%==treplica  ( 
     SET arg_1_Process_1="true"
	 SET arg_1_Process_2="false"
	 SET validFirstArgument=true 
	 )
if %exname_nq%==verify ( 
	 SET validFirstArgument=true 
)
if %validFirstArgument%==false (
  echo The first argument do not correspond to any of the programs: calculator, rogue, treplica, verify
  pause
  EXIT
)	 



echo deleting %projeto_nq%\app\%exname_nq%\projBatchCreated.pyan
del /f /s /q %projeto_nq%\app\%exname_nq%\projBatchCreated.pyan

echo creating the project file %projeto_nq%\app\%exname_nq%\projBatchCreated.pyan


echo import treplica at "%projeto_nq%\lib\treplica"  >> %projeto_nq%\app\%exname_nq%\projBatchCreated.pyan
echo @loadNonDeterministicFiles("%projeto_nq%\app\nonDeterministicFiledToBeLoaded\nonDeter_to_DeterMethod.txt")  >> %projeto_nq%\app\%exname_nq%\projBatchCreated.pyan
echo program >> %projeto_nq%\app\%exname_nq%\projBatchCreated.pyan
echo    package treplica at "%projeto_nq%\lib\treplica" >> %projeto_nq%\app\%exname_nq%\projBatchCreated.pyan
echo    package cyan.math at "%lib_nq%\cyan\math" >> %projeto_nq%\app\%exname_nq%\projBatchCreated.pyan



REM echo %projeto_nq%\app\%exname_nq%\proj.pyan
echo compiling the program

"%lib_nq%\saci"  "%projeto_nq%\app\%exname_nq%\projBatchCreated.pyan" -noexec -java -cp "%projeto_nq%\jar\slf4j-api-1.7.18.jar";"%projeto_nq%\jar\slf4j-jdk14-1.7.18.jar";"%projeto_nq%\jar\treplica-0.1.0.jar" 

IF %ERRORLEVEL% NEQ 0 (
  echo "Error when executing the Cyan compiler"
  pause
  EXIT
)

echo Press any key to execute two copies of the program %exname_nq%
pause

REM java -cp "%projeto_nq%\jar\slf4j-api-1.7.18.jar";"%projeto_nq%\jar\slf4j-jdk14-1.7.18.jar";"%projeto_nq%\jar\treplica-0.1.0.jar";"%projeto_nq%\app\java-for-%exname_nq%";"%lib_nq%\cyan.lang.jar";"%lib_nq%\cyan.math.jar";"%lib_nq%\cyanruntime.jar" main.ProjBatchCreated %arg_0_Process_1% %arg_1_Process_1%

start /B java -cp "%projeto_nq%\jar\slf4j-api-1.7.18.jar";"%projeto_nq%\jar\slf4j-jdk14-1.7.18.jar";"%projeto_nq%\jar\treplica-0.1.0.jar";"%projeto_nq%\app\java-for-%exname_nq%";"%lib_nq%\cyan.lang.jar";"%lib_nq%\cyan.math.jar";"%lib_nq%\cyanruntime.jar" main.ProjBatchCreated %arg_0_Process_1% %arg_1_Process_1%

start /B java -cp "%projeto_nq%\jar\slf4j-api-1.7.18.jar";"%projeto_nq%\jar\slf4j-jdk14-1.7.18.jar";"%projeto_nq%\jar\treplica-0.1.0.jar";"%projeto_nq%\app\java-for-%exname_nq%";"%lib_nq%\cyan.lang.jar";"%lib_nq%\cyan.math.jar";"%lib_nq%\cyanruntime.jar" main.ProjBatchCreated %arg_0_Process_2% %arg_1_Process_2%


goto theEnd
:Noargument
echo This batch file takes four arguments: 
echo     the example name (only the name)
echo     the directory called 'project' 
echo     the 'lib' directory of Cyan 
echo     the 'magic' directory used by treplica. It can be any
echo       directory with the subdirectories 'magic1' and 'magic2'
echo       (do not put the driver!)
echo Example of call:
echo    start /wait call-exem calculator "\Dropbox\research\projeto" "\Dropbox\Cyan\lib" "\Dropbox\19-2\doc\treplica\treplicaDirs"
echo DO NOT use the drive letter as in "C:\Dropbox\19-2\doc\treplica\treplicaDirs" in
echo the last directory. That will cause treplica to thrown a runtime exception
echo DO NOT put a \ after the last subdirectory as in "\Dropbox\Cyan\lib\"

:theEnd