package meta.treplica;

import java.util.List;
import meta.AnnotationArgumentsKind;
import meta.AttachedDeclarationKind;
import meta.CyanMetaobjectAtAnnot;
import meta.ExprReceiverKind;
import meta.ICompiler_dsa;
import meta.IDeclaration;
import meta.Token;
import meta.WrAnnotationAt;
import meta.WrEnv;
import meta.WrExpr;
import meta.WrMessageWithKeywords;
import meta.WrMethodDec;
import meta.WrMethodSignature;
import meta.WrProgramUnit;
import meta.WrSymbol;

/**
 * If a method is annotated with 'nonDeterministic', all superprototype and subprototype
 * methods should also be annotated. And the method can only be called inside
 * methods attached to the same annotation
   @author josed
 */
public class CyanMetaobjectNonDeterministic extends CyanMetaobjectAtAnnot
    implements meta.ICheckOverride_afsa, meta.ICheckDeclaration_afsa,
       meta.ICheckMessageSend_afsa {

	public CyanMetaobjectNonDeterministic() {
		super(nonDeteministicAnnotName,
				AnnotationArgumentsKind.ZeroParameters,
				new AttachedDeclarationKind[] {
						AttachedDeclarationKind.METHOD_DEC },
				Token.PUBLIC);
	}

	public static final String nonDeteministicAnnotName = "nonDeterministic";

	/**
	 * this method is only called if there is a Cyan method annotated with
	 * '{@literal @}nonDeterministic'. If the annotated method is public,
	 *  it checks all superprototype methods with
	 *  the same name as the annotated Cyan method. If any of them is not annotated with
	 * '{@literal @}nonDeterministic', an error is issued. In a hierarchy, either all
	 * methods are deterministic or not deterministic.
	 */
	@Override
	public void afsa_checkDeclaration(ICompiler_dsa compiler) {
		WrEnv env = compiler.getEnv();
		IDeclaration dec = this.getAttachedDeclaration();
		if ( !(dec instanceof WrMethodDec) ) {
			// internal compiler error
			this.addError("Internal compiler error in metaobject '" + this.getName() + "': "
					+ "attached declaration is not a method");
		}
		else {
			String thisMO_name = this.getName();
			WrMethodDec method = (WrMethodDec ) dec;

			if ( method.getVisibility() != Token.PRIVATE ) {
				if ( method.getVisibility() != Token.PUBLIC && method.getVisibility() != Token.PACKAGE ) {
					this.addError("This annotation can only be used with methods with visibility 'public' or 'package'");
				}
				String methodName = method.getName();
				WrProgramUnit pu = method.getDeclaringObject();
				if ( pu.getSuperobject(env) != null ) {
					pu = pu.getSuperobject(env);
					List<WrMethodSignature> superMethodSigList =
							pu.searchMethodPublicPackageSuperPublicPackage(methodName, env);
					if ( superMethodSigList != null && superMethodSigList.size() > 0 ) {
						for ( WrMethodSignature superMethodSig : superMethodSigList ) {
							WrMethodDec superMethod = superMethodSig.getMethod();
							List<WrAnnotationAt> annotList =
									superMethod.getAttachedMetaobjectAnnotationList(env);
							boolean foundND_annotation = false;
							for ( WrAnnotationAt annot : annotList ) {
								if ( annot.getCompleteName().equals(thisMO_name) ) {
									foundND_annotation = true;
									break;
								}
							}
							if ( !foundND_annotation ) {
								this.addError("Method '" + methodName + "' of prototype '"
										+ superMethod.getDeclaringObject().getFullName() + "' " +
										"should have been declared with annotation @" +
										this.getName() + " because the method with the same "
												+ "name in the subprototype '" +
										method.getDeclaringObject().getFullName() + "' " +
										"uses this annotation. In a hierarchy, methods "
										+ "with the same name should be either deterministic or not-deterministic");
							}
						}
					}
				}
			}
		}
	}

	/**
	 * this method is only called if there is a Cyan method annotated with
	 * '{@literal @}nonDeterministic'. If the annotated method is public,
	 *  it checks if a subprototype method with
	 *  the same name is also annotated with this annotation. an error is issued. In a hierarchy, either all
	 * methods are deterministic or not deterministic.
	 */
	@Override
	public void afsa_checkOverride(ICompiler_dsa compiler_dsa,
			WrMethodDec method) {
		WrEnv env = compiler_dsa.getEnv();
		List<WrAnnotationAt> annotList =
				method.getAttachedMetaobjectAnnotationList(env);
		String thisName = this.getName();
		boolean foundAnnot = false;
		for ( WrAnnotationAt annot : annotList ) {
			if ( annot.getCyanMetaobject().getName().equals(thisName) ) {
				foundAnnot = true;
				break;
			}
		}
		if ( ! foundAnnot ) {
			this.addError(method.getFirstSymbol(env),
				"The superprototype method with the same name as this method "
				+ "is annotated with '" + thisName + "'. The metaobject demands "
			    + "that this method should also annotated with '" + thisName + "'");
		}
	}

	@Override
	public void afsa_checkUnaryMessageSendMostSpecific(
			WrExpr receiverExpr, WrProgramUnit receiverType,
			ExprReceiverKind receiverKind, WrSymbol unarySymbol,
			WrProgramUnit mostSpecificReceiver, WrEnv env) {

		checkIfCurrentMethodIsNonDeterministic(receiverExpr, env);
	}

	@Override
	public void afsa_checkKeywordMessageSendMostSpecific(
			WrExpr receiverExpr, WrProgramUnit receiverType,
			ExprReceiverKind receiverKind, WrMessageWithKeywords message,
			WrMethodSignature methodSignature,
			WrProgramUnit mostSpecificReceiver, WrEnv env) {
		checkIfCurrentMethodIsNonDeterministic(receiverExpr, env);

	}

	/**
	   @param receiverExpr
	   @param env
	 */
	private void checkIfCurrentMethodIsNonDeterministic(WrExpr receiverExpr,
			WrEnv env) {
		WrMethodDec m = env.getCurrentMethod();
		List<WrAnnotationAt> annotList = m.getAttachedMetaobjectAnnotationList(env);
		boolean found = false;
		for ( WrAnnotationAt annot : annotList ) {
			String moName = annot.getCyanMetaobject().getName();
			if ( moName.equals(nonDeteministicAnnotName) ||
				 moName.equals(CyanMetaobjectTreplicaAction.treplicaActionMetaobjectName)) {
				found = true;
				break;
			}
		}
		if ( !found ) {
			this.addError(receiverExpr.getFirstSymbol(),
				"This method is calling a non-deterministic method. "
				+ "Therefore it should be annotated with either @" + nonDeteministicAnnotName
				+ " or @" + CyanMetaobjectTreplicaAction.treplicaActionMetaobjectName);
				}
	}
}
