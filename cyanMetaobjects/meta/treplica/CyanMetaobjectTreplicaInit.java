package meta.treplica;

import java.util.List;
import meta.AnnotationArgumentsKind;
import meta.AttachedDeclarationKind;
import meta.CyanMetaobjectAtAnnot;
import meta.IActionVariableDeclaration_dsa;
import meta.IDeclaration;
import meta.WrAnnotationAt;
import meta.WrEnv;
import meta.WrStatementLocalVariableDec;

public class CyanMetaobjectTreplicaInit extends CyanMetaobjectAtAnnot
		implements IActionVariableDeclaration_dsa {

	public CyanMetaobjectTreplicaInit() {
		super("treplicaInit",
				AnnotationArgumentsKind.ZeroOrMoreParameters,
				new AttachedDeclarationKind[] {
						AttachedDeclarationKind.LOCAL_VAR_DEC });
	}


	@Override
	public StringBuffer dsa_codeToAddAfter(WrEnv env) {
		WrAnnotationAt withAt = this.getMetaobjectAnnotation();
		IDeclaration dec = withAt.getDeclaration();
		WrStatementLocalVariableDec varList = (WrStatementLocalVariableDec) dec;

		List<Object> parameterList = withAt.getJavaParameterList();
		String []strList = new String[parameterList.size()];
		int i = 0;
		for ( Object elem : parameterList ) {
			if ( (elem instanceof Integer) ) {
				strList[i] = Integer.toString(((Integer) elem).intValue());
			}else if ( (elem instanceof String) ) {
				strList[i] = (String) elem;
			}
			++i;
		}


		String varname = varList.getName();
		String nameTreplicaVar = "treplica" + varname;
		StringBuffer code = new StringBuffer("var " + nameTreplicaVar + " = Treplica new;\n");
		code.append(nameTreplicaVar + " runMachine: ");
		code.append(varname);
		code.append(" numberProcess: ");
		code.append(strList[0]);
		code.append(" rtt: ");
		code.append(strList[1]);
		code.append(" path: ");
		code.append(strList[2]);
		code.append(";\n");
		code.append(varname + " setTreplica: " + nameTreplicaVar + ";\n");

		return code;
	}

}
