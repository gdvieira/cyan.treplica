package meta.treplica;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import meta.AnnotationArgumentsKind;
import meta.AttachedDeclarationKind;
import meta.CyanMetaobject;
import meta.CyanMetaobjectAtAnnot;
import meta.IAction_dpp;
import meta.ICompiler_dpp;
import meta.IDeclaration;
import meta.Token;
import meta.Tuple2;
import meta.Tuple3;
import meta.WrAnnotationAt;
import meta.WrProgram_dpp;

public class CyanMetaobjectLoadNonDeterministicFiles
		extends CyanMetaobjectAtAnnot
		implements IAction_dpp {

	public static final String loadNonDeterministicFiles = "loadNonDeterministicFiles";

	public CyanMetaobjectLoadNonDeterministicFiles() {
		super("loadNonDeterministicFiles",
				AnnotationArgumentsKind.ZeroOrMoreParameters,
				new AttachedDeclarationKind[] {
						AttachedDeclarationKind.PROGRAM_DEC },
				Token.PUBLIC);
	}

	public static final String treplica_nd_to_determ_map = "treplica_nd_to_determ_map";

	@Override
	public void dpp_action(ICompiler_dpp cp) {
		IDeclaration dec = this.getAttachedDeclaration();
		WrProgram_dpp annotProgram = (WrProgram_dpp ) dec;

		WrAnnotationAt annot = this.getMetaobjectAnnotation();
		List<Object> paramList = annot.getJavaParameterList();
		for ( Object param : paramList ) {
			if ( !(param instanceof String) ) {
				this.addError("All parameters of annotation '" + this.getName() +
						"'" + " should be strings");
				return ;
			}
			String filename = CyanMetaobject.removeQuotes((String ) param);
			List<String> lineList = loadNonDeterministicFile(filename);
			if ( lineList == null ) {
				// error
				return;
			}
			if ( ! buildMapNDcall_to_DetermCall(filename, lineList) ) {
				return ;
			}
		}
		annotProgram.setProgramKeyValue(treplica_nd_to_determ_map, this.nonDeterMessagePassing_to_NewMessagePassingMap);
	}

	private Map<String, Tuple2<
	  Tuple3<String, String, String>,
	  Tuple3<String, String, String>>> mapNDcall_to_DetermCall;

	private boolean buildMapNDcall_to_DetermCall(String filename, List<String> lineList) {

		this.nonDeterMessagePassing_to_NewMessagePassingMap = new HashMap<>();
		this.mapNDcall_to_DetermCall = new HashMap<>();
		int linenumber = 0;
		for (String line : lineList) {
			++linenumber;
			line = line.trim();
			if ( line.length() == 0 ) { continue; }
			String parts[] = line.split("-");
			if ( parts.length != 2 ) {
				this.addError("Error: line " + linenumber + " of "
						+ "file '" + filename + "' is in the wrong format");
				return false;
			}
			String left[] = parts[0].split(",");
			String right[] = parts[1].split(",");
			if ( left.length != 3 || right.length != 3 ) {
				this.addError("Error: line " + linenumber + " of "
						+ "file '" + filename + "' is in the wrong format");
				return false;
			}
			String leftMethodName = left[2].trim();
			String rightMethodName = right[2].trim();
			int colonIndexLeft = leftMethodName.indexOf(':');
			boolean isLeftUnaryMethod = colonIndexLeft < 0;
			int colonIndexRight = rightMethodName.indexOf(':');
			boolean isRightUnaryMethod = colonIndexRight < 0;
			if ( isLeftUnaryMethod != isRightUnaryMethod ) {
				// either both are unary methods or none is
				this.addError("Error: line " + linenumber + " of "
					+ "file '" + filename + "' is in the wrong format. Probably "
					+ "an unary method on the left and a non-unary method on "
					+ "the right. Or vice-versa");
				return false;
			}

			MethodSignatureTreplica mst = null;
			if ( isLeftUnaryMethod ) {
				UnaryMethodSignatureTreplica ums = new UnaryMethodSignatureTreplica();
				ums.methodName = rightMethodName;
				mst = ums;
			}
			else {
				String []rightKeywords = rightMethodName.split(" ");
				String []leftKeywords = leftMethodName.split(" ");
				if ( leftKeywords.length != rightKeywords.length ) {
					this.addError("Error: line " + linenumber + " of "
							+ "file '" + filename + "' is in the wrong format. "
							+ "The number of keywords in both methods are different");
						return false;
				}
				KeywordMethodSignatureTreplica leftMethodSignature = parseMethodName(filename, linenumber, leftKeywords);
				if ( leftMethodSignature == null ) {
					return false;
				}
				KeywordMethodSignatureTreplica rightMethodSignature =
						parseMethodName(filename, linenumber, rightKeywords);
				if ( rightMethodSignature == null ) {
					return false;
				}
				mst = rightMethodSignature;
			}
			mst.packageName = right[0];
			mst.prototypeName = right[1];
			Tuple3<String, String, String> pLeft =
					new Tuple3<String, String, String>(left[0], left[1], leftMethodName);
//			Tuple3<String, String, String> pRight =
//					new Tuple3<String, String, String>(right[0], right[1], rightMethodName);

			String key = composeMapKey(left[0], left[1], leftMethodName);
			if ( this.nonDeterMessagePassing_to_NewMessagePassingMap.put(key,  mst) != null ) {
				this.addError("File '" + filename + "' has a line with the non-deterministic "
						+ "method description '" + key + "'" +
						". However, this description also appears in another file");
			}
//			mapNDcall_to_DetermCall.put(key,
//					new Tuple2<
//					  Tuple3<String, String, String>,
//					  Tuple3<String, String, String>>(pLeft, pRight));
		}
		return true;
	}

	/**
	   @param filename
	   @param linenumber
	   @param leftKeywords
	 */
	private KeywordMethodSignatureTreplica parseMethodName(String filename, int linenumber,
			String[] leftKeywords) {
		KeywordMethodSignatureTreplica kms = new KeywordMethodSignatureTreplica();
		kms.keywordNumParamArray = new KeywordNumParam[leftKeywords.length];
		int i = 0;
		for ( String keywordColonNumber : leftKeywords ) {
			int colonIndex = keywordColonNumber.indexOf(':');
			if ( colonIndex <= 0 ) {
				this.addError("Error: line " + linenumber + " of "
						+ "file '" + filename + "' is in the wrong format. "
						+ "One of the method names is illegal");
					return null;
			}
			String numParamStr = keywordColonNumber.substring(colonIndex + 1);
			if ( numParamStr.length() == 0 ) {
				this.addError("Error: line " + linenumber + " of "
						+ "file '" + filename + "' is in the wrong format. "
						+ "One of the method names is illegal: missing number of parameters after one of the keywords");
				return null;
			}
			kms.keywordNumParamArray[i] = new KeywordNumParam();
			kms.keywordNumParamArray[i].numParam = 0;
			try {
				kms.keywordNumParamArray[i].numParam = Integer.valueOf(numParamStr);
			}
			catch ( NumberFormatException e ) {
				this.addError("Error: line " + linenumber + " of "
						+ "file '" + filename + "' is in the wrong format. "
						+ "One of the method names is illegal: the number of parameters after one of the keywords is not correct");
				return null;
			}
			String keyword = keywordColonNumber.substring(0, colonIndex);
			for (int k = 0; k < keyword.length(); ++k) {
				char ch = keyword.charAt(k);
				if ( !Character.isAlphabetic(ch) && ch != '_' ) {
					this.addError("Error: line " + linenumber + " of "
							+ "file '" + filename + "' is in the wrong format. "
							+ "One of the method names uses character '" + ch + "' that is illegal in method names");
					return null;

				}
			}
			kms.keywordNumParamArray[i].keyword = keyword + ":";
			++i;
		}
		return kms;
	}


	public static String composeMapKey(String packageName,
			String prototypeName, String methodName) {
		return packageName.trim() + "." + prototypeName.trim() + " " + methodName.trim();
	}


	private List<String> loadNonDeterministicFile(
			String filename) {

		List<String> list = new ArrayList<>();

		try (BufferedReader br = Files.newBufferedReader(Paths.get(filename))) {
			list = br.lines().collect(Collectors.toList());
		} catch (IOException e) {
			this.addError("In metaobject '" + this.getName() + "', error when reading file '" + filename + "'");
			return null;
		}
		return list;
	}


	private Map<String, MethodSignatureTreplica> nonDeterMessagePassing_to_NewMessagePassingMap;

//	@SuppressWarnings("unused")
//	private static MethodSignatureTreplica soBothAreLoadedbyTheSameLoader1 =
//			new UnaryMethodSignatureTreplica();
//	@SuppressWarnings("unused")
//	private static MethodSignatureTreplica soBothAreLoadedbyTheSameLoader2 =
//			new KeywordMethodSignatureTreplica();
//
//	@SuppressWarnings("unused")
//	private static MethodSignatureTreplica soBothAreLoadedbyTheSameLoader3 =
//			new MethodSignatureTreplica();
//
//	static {
//		for ( StackTraceElement e : Thread.currentThread().getStackTrace()) {
//			System.out.println(e.getClassName() + "::" + e.getMethodName() + " ln: " + e.getLineNumber());
//		}
//		System.out.println("soBothAreLoadedbyTheSameLoader");
//
//		System.out.println(soBothAreLoadedbyTheSameLoader1.getClass().getName());
//		System.out.println(soBothAreLoadedbyTheSameLoader2.getClass().getName());
//		System.out.println(soBothAreLoadedbyTheSameLoader3.getClass().getName());
//
//	}
}




class NonDeterm_DetermMethodMap {
	public MethodSignatureTreplica leftToBeReplacedMethodSig;
	public MethodSignatureTreplica rightToReplaceMethodSig;
}

class MethodSignatureTreplica {
	public String packageName;
	public String prototypeName;
}

class UnaryMethodSignatureTreplica extends MethodSignatureTreplica {
	public String methodName;
}


class KeywordMethodSignatureTreplica extends MethodSignatureTreplica {
	public KeywordNumParam []keywordNumParamArray;
}

/*
 *  represents a keyword of a method and its number of parameters, like
 *  	at:1
 *  of the signature
 *  	at:1 with:2
 */
class KeywordNumParam {
	public String keyword;
	public int numParam;
}