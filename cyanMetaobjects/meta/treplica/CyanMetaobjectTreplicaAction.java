package meta.treplica;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import meta.AnnotationArgumentsKind;
import meta.AttachedDeclarationKind;
import meta.CyanMetaobject;
import meta.CyanMetaobjectAtAnnot;
import meta.IActionNewPrototypes_afti;
import meta.IAction_afti;
import meta.IAction_dsa;
import meta.ICheckDeclaration_afsa;
import meta.ICompiler_afti;
import meta.ICompiler_dsa;
import meta.IDeclaration;
import meta.ISlotInterface;
import meta.IdentStarKind;
import meta.MetaHelper;
import meta.Tuple2;
import meta.WrASTVisitor;
import meta.WrAnnotation;
import meta.WrAnnotationAt;
import meta.WrEnv;
import meta.WrExpr;
import meta.WrExprIdentStar;
import meta.WrExprMessageSendUnaryChainToExpr;
import meta.WrExprMessageSendUnaryChainToSuper;
import meta.WrExprMessageSendWithKeywords;
import meta.WrExprMessageSendWithKeywordsToExpr;
import meta.WrExprMessageSendWithKeywordsToSuper;
import meta.WrMessageKeywordWithRealParameters;
import meta.WrMethodDec;
import meta.WrMethodKeywordWithParameters;
import meta.WrMethodSignature;
import meta.WrMethodSignatureOperator;
import meta.WrMethodSignatureUnary;
import meta.WrMethodSignatureWithKeywords;
import meta.WrParameterDec;
import meta.WrProgram;
import meta.WrProgramUnit;

enum CheckReplace { CHECK_NON_DETERMINISM,
	REPLACE_NON_DETERM_CALL_BY_DETERM_CALL }

public class CyanMetaobjectTreplicaAction
		extends CyanMetaobjectAtAnnot
		implements IAction_afti, IAction_dsa, ICheckDeclaration_afsa,
					IActionNewPrototypes_afti {
	// ,
	// ICheckProgramUnit_dsa2
	// {

	public static final String treplicaActionMetaobjectName = "treplicaAction";

	public CyanMetaobjectTreplicaAction() {
		super(treplicaActionMetaobjectName,
				AnnotationArgumentsKind.ZeroParameters,
				new AttachedDeclarationKind[] {
						AttachedDeclarationKind.METHOD_DEC });
//		visited = new Vector<>();
//		methodsND = new Vector<>();
//		loadedPack = new Vector<String>();
	}

	public static String substringBeforeLast(String str, String separator) {
		int pos = str.lastIndexOf(separator);
		if (pos == -1) {
			return str;
		}
		return str.substring(0, pos);
	}

//	public void actionNonDeterminism( CheckReplace checkReplace, WrExprMessageSend node,
//			WrMethodDec method, String prototypeName, String methodName,
//			WrEnv env) {
//		loadTreplicaNd(
//				method.getDeclaringObject().getCompilationUnit(env).getPackageName() );
//		methodName = methodName.replaceAll(":", "");
//		String parameters = "";
//		if (node != null) {
//			String [] splitParam = node.asString().split(":", 2);
//			if(splitParam.length == 2) {
//				parameters = ":" + splitParam[1];
//			}
//		}
//		//System.out.println("parameters: " + parameters);
//		//System.out.println("test pack: " + pack + " func: " + func);
//		Tuple2<String, String> left =
//				new Tuple2<String, String>(prototypeName, methodName);
//		Tuple3<String, String, String> right = null;
//		/*
//		 * [. [. packageName, prototypeName, methodName .],
//		 *    [. packageName, prototypeName, methodName .]
//		 * .]
//		 */
//		String packageNameND = null;
//		String prototypeNameND = null;
//		String methodNameND = null;
//		for ( Tuple2<Tuple3<String, String, String>,
//				Tuple3<String, String, String>> pairFunc :
//					methodsND) {
//			packageNameND = pairFunc.f1.f1;
//			prototypeNameND = pairFunc.f1.f2;
//			methodNameND = pairFunc.f1.f3;
//			if( !packageNameND.equals(
//					substringBeforeLast(method.getDeclaringObject().getFullName(), ".")) ) {
//				break;
//			}
//			if (  prototypeNameND.equals(left.f1)
//					&& methodNameND.equals(left.f2)) {
//				right = pairFunc.f2;
//				break;
//			}
//		}
//		if (right != null) {
//
//			if ( CheckReplace.REPLACE_NON_DETERM_CALL_BY_DETERM_CALL == checkReplace ) {
//				//System.out.println("replace: " + right.getKey() + " " + right.getValue() + parameters);
//				//System.out.println("![ " + right.getValue().trim() + " ]!");
//				//System.out.println("![ " + parameters + " ]!");
//				StringBuffer codeToAdd = new StringBuffer(
//						right.f1 + "." + right.f2 + " " +
//								right.f3.trim() + parameters );
//				//System.out.println("![ " + codeToAdd + " ]!");
//				if (node != null) {
//					compiler.replaceStatementByCode(
//							node,
//							this.getMetaobjectAnnotation(), codeToAdd, node.getType()
//							);
//					//compiler.removeAddCodeExprMessageSend(node, this.getMetaobjectAnnotation(), codeToAdd, node.getType());
//					//System.out.println("replace end");
//				}
//			}
//			else {
//				if ( packageNameND != null ) {
//					this.addError("Found a non-deterministic method call. A message send may call method '" +
//							methodName +
//							"' of prototype '" + packageNameND + "." +
//							prototypeNameND + "' that is non-deterministic");
//				}
//			}
//		}
//	}

//	public boolean visitedContain(WrMethodDec item, WrEnv env) {
//		String packItem = item.getDeclaringObject()
//				.getCompilationUnit(env).getNamePublicPrototype();
//		String funcItem = item.getNameWithoutParamNumber();
//
//		for (WrMethodDec method : visited) {
//			String packTemp = method.getDeclaringObject().getCompilationUnit(env).getNamePublicPrototype();
//			String funcTemp = method.getNameWithoutParamNumber();
//
//			if (packTemp.equals(packItem) && funcTemp.equals(funcItem)) {
//				return true;
//			}
//		}
//		return false;
//	}

//	public void visitorWalk(CheckReplace checkReplace,
//			WrExprMessageSend node,
//			String packageName1, String prototypeName, String methodName,
//			WrEnv env) {
//		//		Pair<String, String> methodPair = new Pair<String, String>(prototypeName, methodName);
//		for ( WrMethodDec method : findMethod(packageName1, prototypeName, methodName) ) {
//			if ( !visitedContain(method, env) ) {
//				actionNonDeterminism(checkReplace, node, method,
//						prototypeName, methodName, env);
//				visitorFunctionsTree(checkReplace, method, this, env);
//			}
//		}
//	}
//

	@SuppressWarnings("unchecked")
	public void visitorFunctionsTree(CheckReplace checkReplace,
			WrMethodDec method, CyanMetaobjectTreplicaAction metaobject, WrEnv env) {
//		visited.addElement(method);
		//String annotName = metaobject.getName();
		WrProgram program2 = env.getProject().getProgram();
		Object obj2ObjMap = program2.getProgramValueFromKey(CyanMetaobjectLoadNonDeterministicFiles.treplica_nd_to_determ_map);
		if ( !(obj2ObjMap instanceof Map<?, ?> ) ) {
			 //mapNDcall_to_DetermCall = (Map<String, Tuple2<Tuple3<String, String, String>, Tuple3<String, String, String>>> ) obj2ObjMap;
			this.addError("Internal error in visitorFunctionsTree: map cannot be cast to the correct type");
			return ;
		}
		nonDeterMessagePassing_to_NewMessagePassingMap = (Map<String, MethodSignatureTreplica> ) obj2ObjMap;
		WrAnnotationAt metaobjectAnnot = metaobject.getMetaobjectAnnotation();
		method.accept(new WrASTVisitor() {
			@Override
			public void visit(
					WrExprMessageSendWithKeywordsToExpr node, WrEnv env1) {

//				// visiting a message passing whose receiver is an expression
//				WrType receiverType;
//				if ( node.getReceiverExpr() == null ) {
//					// without the receiver, the receiver is self
//					receiverType = env.getCurrentProgramUnit();
//				}
//				else {
//					if ( node.getReceiverExpr().getType() == null ) {
//						metaobject.addError("The type of the expression '" + node.asString() + "' was not found");
//						return ;
//					}
//					else {
//						receiverType = node.getReceiverExpr().getType();
//					}
//				}
//				if ( !(receiverType instanceof meta.WrProgramUnit) ||
//						((WrProgramUnit ) receiverType).isInterface() ) {
//					return ;
//				}
				checkReplaceKeywordMessagePassing(node);

//				WrProgramUnit pu = ((WrProgramUnit ) receiverType);
//				String packageName1 = pu.getCompilationUnit(env)
//						.getPackageName();
//				String prototypeName = pu.getName();
//				//env.search
//				visitorWalk(checkReplace, node, packageName1,
//						prototypeName, methodName, env);
			}

			/**
			   @param checkReplace
			   @param metaobject
			   @param env
			   @param metaobjectAnnot
			   @param node
			 */
			private void checkReplaceKeywordMessagePassing(
					WrExprMessageSendWithKeywords node) {
				WrMethodSignature methodSig = node.getMethodSignatureForMessage();
				WrMethodDec methodForMessage = methodSig.getMethod();
				WrProgramUnit progUnitMethod = methodForMessage.getDeclaringObject();
				String packageName_puMethod = progUnitMethod.getPackageName();
				String methodName = methodForMessage.getName();
				boolean foundNDannot = hasNonDeterministicAnnotation(env,
						methodForMessage);
				// search for a replacement for this non-deterministic method call
				String key = CyanMetaobjectLoadNonDeterministicFiles.composeMapKey(
				    packageName_puMethod, progUnitMethod.getName(), methodName);

				MethodSignatureTreplica messageSendToReplaceCurrent = metaobject.nonDeterMessagePassing_to_NewMessagePassingMap.get(key);
				/*
				 * errors if:
				 *     . CHECK_NON_DETERMINISM and value is not null; that is, the method is
				 *       in the list to be replaced, it should have been replaced before
				 *     . REPLACE_NON_DETERM_CALL_BY_DETERM_CALL and the method is annotated
				 *       with '@nonDeterministic' and value is null.
				 */
				switch ( checkReplace  ) {
				case CHECK_NON_DETERMINISM:
					if ( messageSendToReplaceCurrent != null ) {
						// a non-deterministic method is in the code in phase afsa
						metaobject.addError(node.getFirstSymbol(), "Message passing '" +
						   node.asString() + "' can call a non-deterministic method. It should have"
						   		+ " been replaced in a previous compilation phase");
						return ;
					}
					break;
				case REPLACE_NON_DETERM_CALL_BY_DETERM_CALL:
					if ( messageSendToReplaceCurrent == null ) {
						if ( foundNDannot ) {
							metaobject.addError(node.getFirstSymbol(), "Message passing '" +
									   node.asString() + "' can call a non-deterministic method. "
									   		+ "However, there is no method it should be replaced to. "
									   		+ "The method should have been specified in a file whose name "
									   		+ "is a parameter to annotation '@" +
									   		CyanMetaobjectLoadNonDeterministicFiles.loadNonDeterministicFiles
									   		+ "' in the project file");
									return ;
						}
					}
					else {
						if ( !(messageSendToReplaceCurrent instanceof KeywordMethodSignatureTreplica) ) {
							metaobject.addError("The message passing \n" +
								     node.asString() + "\nmay call a non-deterministic method. "
								     		+ "However, the message passing is a keyword message and the replacement method is a unary method");
									return ;
						}
						KeywordMethodSignatureTreplica replacementMetSig = (KeywordMethodSignatureTreplica ) messageSendToReplaceCurrent;

						List<WrMessageKeywordWithRealParameters> keyParamList = node.getMessage().getkeywordParameterList();
						String newMessagePassing = "( " + replacementMetSig.packageName + "." +
								replacementMetSig.prototypeName +  " ";
						int i = 0;
						for (WrMessageKeywordWithRealParameters keywordParams : keyParamList ) {
							newMessagePassing += " " + replacementMetSig.keywordNumParamArray[i].keyword + " ";
							int sizeArgs = keywordParams.getExprList().size();
							for ( WrExpr realArg : keywordParams.getExprList() ) {
								newMessagePassing += realArg.asString();
								if ( --sizeArgs > 0 ) {
									newMessagePassing += ", ";
								}
							}
							++i;
						}
						newMessagePassing += " ) ";
						compiler.replaceStatementByCode(node,
								metaobjectAnnot,
								new StringBuffer(newMessagePassing),
								methodForMessage.getMethodSignature().getReturnType(env));

					}
					break;
				default:
					metaobject.addError("Internal error in metaobject '" + metaobject.getName() +
							"': enum CheckReplace has more than two elements");
				}
			}

			private void checkReplaceUnaryMessagePassing(
					WrExpr node, WrMethodSignature methodSig) {
				WrMethodDec methodForMessage = methodSig.getMethod();
				WrProgramUnit progUnitMethod = methodForMessage.getDeclaringObject();
				String packageName_puMethod = progUnitMethod.getPackageName();
				String methodName = methodSig.getName();
				boolean foundNDannot = hasNonDeterministicAnnotation(env,
						methodForMessage);
				// search for a replacement for this non-deterministic method call
				String key = CyanMetaobjectLoadNonDeterministicFiles.composeMapKey(
				    packageName_puMethod, progUnitMethod.getName(), methodName);

				MethodSignatureTreplica messageSendToReplaceCurrent = metaobject.nonDeterMessagePassing_to_NewMessagePassingMap.get(key);
				/*
				 * errors if:
				 *     . CHECK_NON_DETERMINISM and value is not null; that is, the method is
				 *       in the list to be replaced, it should have been replaced before
				 *     . REPLACE_NON_DETERM_CALL_BY_DETERM_CALL and the method is annotated
				 *       with '@nonDeterministic' and value is null.
				 */
				switch ( checkReplace  ) {
				case CHECK_NON_DETERMINISM:
					if ( messageSendToReplaceCurrent != null ) {
						// a non-deterministic method is in the code in phase afsa
						metaobject.addError(node.getFirstSymbol(), "Message passing '" +
						   node.asString() + "' can call a non-deterministic method. It should have"
						   		+ " been replaced in a previous compilation phase");
						return ;
					}
					break;
				case REPLACE_NON_DETERM_CALL_BY_DETERM_CALL:
					if ( messageSendToReplaceCurrent == null ) {
						if ( foundNDannot ) {
							metaobject.addError(node.getFirstSymbol(), "Message passing '" +
									   node.asString() + "' can call a non-deterministic method. "
									   		+ "However, there is no method it should be replaced to. "
									   		+ "The method should have been specified in a file whose name "
									   		+ "is a parameter to annotation '@" +
									   		CyanMetaobjectLoadNonDeterministicFiles.loadNonDeterministicFiles
									   		+ "' in the project file");
									return ;
						}
					}
					else {
						if ( !(messageSendToReplaceCurrent instanceof UnaryMethodSignatureTreplica) ) {
							metaobject.addError("The message passing \n" +
								     node.asString() + "\nmay call a non-deterministic method. "
								     		+ "However, the message passing is an unary message and the replacement method is a keyword method");
									return ;
						}
						UnaryMethodSignatureTreplica replacementMetSig = (UnaryMethodSignatureTreplica ) messageSendToReplaceCurrent;

						String newMessagePassing = "( " + replacementMetSig.packageName + "." +
								replacementMetSig.prototypeName + " " +
						    replacementMetSig.methodName + " ) ";
						compiler.replaceStatementByCode(node,
								metaobjectAnnot,
								new StringBuffer(newMessagePassing),
								methodForMessage.getMethodSignature().getReturnType(env));

					}
					break;
				default:
					metaobject.addError("Internal error in metaobject '" + metaobject.getName() +
							"': enum CheckReplace has more than two elements");
				}
			}


			@Override
			public void visit(WrExprMessageSendWithKeywordsToSuper node, WrEnv env1) {
//				WrType type = node.getSuperobject().getType();
//				if ( !(type instanceof meta.WrProgramUnit) ||
//						((WrProgramUnit ) type).isInterface() ) {
//					return ;
//				}
//				String packageName1 = ((WrProgramUnit ) type).getCompilationUnit(env1).getPackageName();
//
//				String prototypeName = node.getSuperobject().getType().getName();
//				String methodName = node.getMessage().getMethodName();
				checkReplaceKeywordMessagePassing(node);

//				visitorWalk(checkReplace, node, packageName1, prototypeName, methodName, env1);
			}


			@Override
			public void visit(WrExprIdentStar node, WrEnv env1) {
				if (node.getIdentStarKind() == IdentStarKind.unaryMethod_t) {
					checkReplaceUnaryMessagePassing(node, node.getMethodSignatureForMessageSend());

//					WrProgramUnit programUnit = ((WrCompilationUnit) node.getFirstSymbol().getCompilationUnit()).getPublicPrototype();
//					if ( programUnit.isInterface() ) {
//						return ;
//					}
//					String packageName1 = programUnit.getCompilationUnit(env1).getPackageName();
//
//					String prototypeName = ((WrCompilationUnit) node.getFirstSymbol().getCompilationUnit())
//							.getNamePublicPrototype();
//					String methodName = node.getName();
//					visitorWalk(checkReplace, null, packageName1, prototypeName, methodName, env1);
				}
			}

			@Override
			public void visit(WrExprMessageSendUnaryChainToExpr node, WrEnv env1) {
				checkReplaceUnaryMessagePassing(node, node.getMethodSignatureForMessageSend());
//				if( node.getReceiver().getType() != null){
//					WrType type = node.getReceiver().getType();
//					if ( !(type instanceof meta.WrProgramUnit) ||
//							((WrProgramUnit ) type).isInterface() ) {
//						return ;
//					}
//
//					String packageName1 = ((WrProgramUnit ) type).getCompilationUnit(env1).getPackageName();
//					String prototypeName = node.getReceiver().getType().getName();
//					String methodName = node.getMessageName();
//					visitorWalk(checkReplace, node, packageName1, prototypeName, methodName, env1);
//				}
			}

			@Override
			public void visit(WrExprMessageSendUnaryChainToSuper node, WrEnv env1) {
				checkReplaceUnaryMessagePassing(node, node.getMethodSignatureForMessageSend());
//				WrType type = node.getReceiver().getType();
//				if ( !(type instanceof meta.WrProgramUnit) ||
//						((WrProgramUnit ) type).isInterface() ) {
//					return ;
//				}
//				String packageName1 = ((WrProgramUnit ) type).getCompilationUnit(env1).getPackageName();
//
//				String prototypeName = node.getReceiver().getType().getName();
//				String methodName = node.getMessageName();
//				visitorWalk(checkReplace, node, packageName1, prototypeName, methodName, env1);
			}
		}, env);
	}


//	public List<WrMethodDec> findMethod(
//			String packageName1, String prototypeName, String methodName
//			) {
//		System.out.println("This method, findMethod, should not be called !!!");
//		return null;
//	}

	//	public List<WrMethodDec> findMethod(
	//			String packageName1, String prototypeName, String methodName
	//			) {
	//		List<WrMethodDec> list = new ArrayList<WrMethodDec>();
	//
	//		WrCyanPackage cyanPackage = null;
	//		for (WrCyanPackage cp : program.getPackageList() ) {
	//			if ( cp.getPackageName().equals(packageName1) ) {
	//				cyanPackage = cp;
	//				break;
	//			}
	//		}
	//		if ( cyanPackage == null ) {
	//			// an internal error
	//			return new ArrayList<WrMethodDec>();
	//		}
	//
	//		ProgramUnit programUnit = cyanPackage.searchPublicNonGenericProgramUnit(prototypeName);
	//		if ( programUnit != null ) {
	//			// this must be the case, otherwise, there is a compiler internal error
	//
	//			Set<ProgramUnit> subProtoList = this.mapProtoSubProto
	//					.get(packageName1 + " " + prototypeName);
	//			if ( subProtoList != null ) {
	//				for (ProgramUnit subProt : subProtoList) {
	//					ObjectDec dec = (ObjectDec) subProt;
	//					for (WrMethodDec method : dec.getMethodDecList()) {
	//						if (method.getNameWithoutParamNumber().equals(
	//								methodName)) {
	//							list.add(method);
	//						}
	//					}
	//				}
	//			}
	//
	//			ObjectDec dec = (ObjectDec) programUnit;
	//			for (WrMethodDec method : dec.getMethodDecList()) {
	//				if (method.getNameWithoutParamNumber().equals(methodName)) {
	//					list.add(method);
	//				}
	//			}
	//
	//		}
	////		for (CompilationUnit compilerUnit : cyanPackage.getCompilationUnitList()) {
	////			for (ProgramUnit programUnit : compilerUnit.getProgramUnitList()) {}
	////		}
	//
	//
	//		return list;
	//	}
	//


	//	public List<WrMethodDec> findMethod(Pair<String, String> name) {
	//		List<WrMethodDec> list = new ArrayList<WrMethodDec>();
	//		for (CyanPackage pack : program.getPackageList()) {
	//			if (!pack.getPackageName().equals("cyan.lang")) {
	//				for (CompilationUnit compilerUnit : pack.getCompilationUnitList()) {
	//					for (ProgramUnit programUnit : compilerUnit.getProgramUnitList()) {
	//						Set<ProgramUnit> subProtoList = this.mapProtoSubProto
	//								.get(pack.getPackageName() + " " + programUnit.getName());
	//						for (ProgramUnit subProt : subProtoList) {
	//							ObjectDec dec = (ObjectDec) subProt;
	//							for (WrMethodDec method : dec.getMethodDecList()) {
	//								if (method.getNameWithoutParamNumber().equals(name.getValue())) {
	//									list.add(method);
	//								}
	//							}
	//						}
	//
	//						ObjectDec dec = (ObjectDec) programUnit;
	//						for (WrMethodDec method : dec.getMethodDecList()) {
	//							if (method.getNameWithoutParamNumber().equals(name.getValue())) {
	//								list.add(method);
	//							}
	//						}
	//					}
	//				}
	//			}
	//		}
	//		return list;
	//	}


	@Override
	public Tuple2<StringBuffer, String> afti_codeToAdd(
			ICompiler_afti compiler, List<Tuple2<WrAnnotation,
			List<ISlotInterface>>> infoList) {
		//	List<Tuple2<String, StringBuffer>> ati_codeToAddToPrototypes(
		//			ICompiler_ati compiler)
		// Method Name
		WrMethodDec md = (WrMethodDec) this.getAttachedDeclaration();
		String methodName = md.getNameWithoutParamNumber();
		String methodSigStr;

		WrMethodSignature methodSig = md.getMethodSignature();
		methodSigStr = strMethodSignature(methodSig);



		// ************************************************

		methodName = methodName.replaceAll(":", "");

		String funcPar = "";
		String hasFunPar = " ";
		String callPar = "";
		String hasCallPar = "";
		String funcRet = "";
		String hasCallRet = "";
		String retStatement = "";
		if ( md.getMethodSignature().getParameterList() != null ) {
			for (WrParameterDec par : md.getMethodSignature().getParameterList()) {
				funcPar += " " + par.getType().getName() + " " + par.getName() + ",";
				callPar += " " + par.getName() + ",";
			}
		}
		if ( md.getMethodSignature().getReturnTypeExpr() != null) {
			hasCallRet = " -> ";
			funcRet = md.getMethodSignature().getReturnTypeExpr().getType().getName();
			retStatement = "@javacode<<<\n" + "return ("
					+ md.getMethodSignature().getReturnTypeExpr().getType().getJavaName() + ")_ret;\n" + ">>>\n";
		}

		if (funcPar.length() > 0) {
			funcPar = funcPar.substring(0, funcPar.length() - 1);
			hasFunPar = ": ";
		}
		if (callPar.length() > 0) {
			callPar = callPar.substring(0, callPar.length() - 1);
			hasCallPar = ": ";
		}

		// Object Name
		String objectName = compiler.getCurrentPrototypeName();
		String prototypeName = CyanMetaobject.removeQuotes(objectName + methodName);
		methodSigStr = "func " + methodSigStr + hasCallRet + funcRet;
		StringBuffer code = new StringBuffer("\n\n    " + methodSigStr +
				" {\n" + "var action = " + prototypeName + " new" + hasCallPar + callPar + ";\n"
				+ "var Dyn ret = getTreplica execute: action;\n" + retStatement + "}\n");
		//		List<Tuple2<String, StringBuffer>> result = new ArrayList<>();
		//		result.add(new Tuple2<String, StringBuffer>(objectName, code));
		Tuple2<StringBuffer, String> r = new Tuple2<>(code, methodSigStr);

		return r;
	}

	/**
	   @param methodSigStr
	   @param methodSig
	   @return
	 */
	private String strMethodSignature( WrMethodSignature methodSig ) {
		String methodSigStr;
		if ( methodSig instanceof WrMethodSignatureUnary ) {
			methodSigStr = ((WrMethodSignatureUnary ) methodSig).getName();
		}
		else if ( methodSig instanceof WrMethodSignatureWithKeywords ) {
			WrMethodSignatureWithKeywords kms = (WrMethodSignatureWithKeywords ) methodSig;
			List<WrMethodKeywordWithParameters> keywordArray = kms.getKeywordArray();
			methodSigStr = "";
			for ( WrMethodKeywordWithParameters keyword : keywordArray ) {
				methodSigStr += keyword.getName() + " ";
				List<WrParameterDec> paramList = keyword.getParameterList();
				if ( paramList != null ) {
					int size = paramList.size();
					for (WrParameterDec param : paramList ) {
						methodSigStr += param.getType().getFullName() + " " + param.getName();
						if ( --size > 0 ) {
							methodSigStr += ", ";
						}
					}
				}
				methodSigStr += " ";
			}
		}
		else if ( methodSig instanceof WrMethodSignatureOperator ) {
			WrMethodSignatureOperator oms = ( WrMethodSignatureOperator ) methodSig;
			WrParameterDec param = oms.getOptionalParameter();
			if ( param == null ) {
				methodSigStr = oms.getName();
			}
			else {
				methodSigStr = oms.getName() + " " + param.getType().getFullName() + " " + param.getName();
			}
		}
		else {
			this.addError("Internal error in afti_renameMethod of metaobject '" + this.getName() +
					"': unknown subclass of WrMethodSignature");
			return null;
		}
		return methodSigStr;
	}

	@Override
	public
	//List<Tuple3<String, String, String[]>> ati_renameMethod(ICompiler_ati compiler_ati)
	List<Tuple2<String, String []>> afti_renameMethod(
			ICompiler_afti compiler_afti) {
		WrMethodDec md = (WrMethodDec) this.getAttachedDeclaration();
		String oldMethodName = md.getName();
		if ( fixName == null ) {
			this.fixName = compiler_afti.nextIdentifier();
		}

		String[] newMethodNameWithPar = createNewMethodNameWithPar(md);

		List<Tuple2<String, String[]>> tupleList = new ArrayList<>();
		tupleList.add(new Tuple2<String, String[]>(oldMethodName, newMethodNameWithPar));
		return tupleList;
	}

	/**
	   @param compiler_afti
	   @param md
	   @param methodSig
	   @return
	 */
	private String[] createNewMethodNameWithPar(WrMethodDec md) {
		WrMethodSignature methodSig = md.getMethodSignature();

		String newMethodNameWithPar[] = null;

		if ( methodSig instanceof WrMethodSignatureUnary ) {
			newMethodNameWithPar = new String[] { newMethodName(md.getNameWithoutParamNumber()) };
		}
		else if ( methodSig instanceof WrMethodSignatureWithKeywords ) {
			WrMethodSignatureWithKeywords kms = (WrMethodSignatureWithKeywords ) methodSig;
			List<WrMethodKeywordWithParameters> keywordArray = kms.getKeywordArray();
			newMethodNameWithPar = new String[kms.getKeywordArray().size()];
			int i = 0;
			for ( WrMethodKeywordWithParameters keyword : keywordArray ) {
				if ( i == 0 ) {
					String name = keyword.getName();
					newMethodNameWithPar[0] = newMethodName(name.substring(0, name.length() - 1 )) + ":";
				}
				else {
					newMethodNameWithPar[i] = keyword.getName();
				}
				++i;
			}
		}
		else if ( methodSig instanceof WrMethodSignatureOperator ) {
			WrMethodSignatureOperator oms = ( WrMethodSignatureOperator ) methodSig;
			//WrParameterDec param = oms.getOptionalParameter();
			newMethodNameWithPar = new String[] {
					newMethodName(MetaHelper.alphaName(oms.getName()))
					} ;
		}
		else {
			this.addError("Internal error in afti_renameMethod of metaobject '" + this.getName() +
					"': unknown subclass of WrMethodSignature");
			return null;
		}
		return newMethodNameWithPar;
	}


	private String strMethodCall( WrMethodDec methodDec, WrMethodSignature methodSig ) {
		String methodSigStr;
		if ( methodSig instanceof WrMethodSignatureUnary ) {
			methodSigStr = createNewMethodNameWithPar(methodDec)[0];
		}
		else if ( methodSig instanceof WrMethodSignatureWithKeywords ) {
			WrMethodSignatureWithKeywords kms = (WrMethodSignatureWithKeywords ) methodSig;
			List<WrMethodKeywordWithParameters> keywordArray = kms.getKeywordArray();
			methodSigStr = "";
			String[] newMethodNameWithPar = createNewMethodNameWithPar(methodDec);

			int i = 0;
			for ( WrMethodKeywordWithParameters keyword : keywordArray ) {
				methodSigStr += newMethodNameWithPar[i] + " ";
				List<WrParameterDec> paramList = keyword.getParameterList();
				if ( paramList != null ) {
					int size = paramList.size();
					for (WrParameterDec param : paramList ) {
						methodSigStr += param.getName() + "Var";
						if ( --size > 0 ) {
							methodSigStr += ", ";
						}
					}
				}
				methodSigStr += " ";
				++i;
			}
		}
		else if ( methodSig instanceof WrMethodSignatureOperator ) {
			WrMethodSignatureOperator oms = ( WrMethodSignatureOperator ) methodSig;
			WrParameterDec param = oms.getOptionalParameter();
			methodSigStr = createNewMethodNameWithPar(methodDec)[0];
//			if ( param == null ) {
//				methodSigStr = oms.getName();
//			}
//			else {
//				methodSigStr = param.getName() + "Var";
//			}
		}
		else {
			this.addError("Internal error in afti_renameMethod of metaobject '" + this.getName() +
					"': unknown subclass of WrMethodSignature");
			return null;
		}
		return methodSigStr;
	}


	@Override
	public
	//List<Tuple2<String, StringBuffer>> ati_NewPrototypeList(ICompiler_ati compiler_ati)
	List<Tuple2<String, StringBuffer>> afti_NewPrototypeList(ICompiler_afti compiler_afti) {
		// Method Name
		WrMethodDec methodDec = (WrMethodDec) this.getAttachedDeclaration();
		String methodName = methodDec.getNameWithoutParamNumber();
		methodName = methodName.replaceAll(":", "");
		WrMethodSignature methodSignature = methodDec.getMethodSignature();

		String varDef = "";
		String funcPar = "";
		String hasFuncPar = " ";
		String funcAssign = "";
		String callPar = "";
		String hasCallPar = "";
		String retStatement = "";
		String retNullState = "";
		if ( methodSignature.getParameterList() != null ) {
			for (WrParameterDec par : methodDec.getMethodSignature().getParameterList()) {
				varDef += "var " + par.getType().getName() + " " + par.getName() + "Var\n";
				funcPar += " " + par.getType().getName() + " " + par.getName() + ",";
				funcAssign += par.getName() + "Var = " + par.getName() + ";\n";
				callPar += " " + par.getName() + "Var,";
			}
		}

		if (methodDec.getMethodSignature().getReturnTypeExpr() != null) {
			retStatement = "return ";
		} else {
			retNullState = "return Nil;\n";
		}

		if (funcPar.length() > 0) {
			funcPar = funcPar.substring(0, funcPar.length() - 1);
			hasFuncPar = ": ";
		}
		if (callPar.length() > 0) {
			callPar = callPar.substring(0, callPar.length() - 1);
			hasCallPar = ": ";
		}

		// Object Name
		String objectName = compiler_afti.getCurrentPrototypeName();

		List<Tuple2<String, StringBuffer>> protoCodeList = new ArrayList<>();
		String prototypeName = CyanMetaobject.removeQuotes(objectName + methodName);

		if ( fixName == null ) {
			this.fixName = compiler_afti.nextIdentifier();
		}

		String s = strMethodCall(methodDec, methodSignature);

		StringBuffer code = new StringBuffer(

			"package main\n" + "\n" + "import treplica\n" + "\n" +
			"object " + prototypeName + " extends Action\n\n"
			+ varDef + "\n" + "func init" + hasFuncPar + funcPar + " {\n" + funcAssign + "}\n" + "\n"
			+ "    override\n" + "    func executeOn: Context context -> Dyn {\n" +
			"        var obj = Cast<" + objectName
			+ "> asReceiver: context;\n" + retStatement +
			"        obj " + s + //this.newMethodName(methodName)
			//+ hasCallPar + callPar +
			";\n        " + retNullState +
			"    }\n" +
			"end\n"

				);
		String unescape = CyanMetaobject.unescapeString(code.toString());
		protoCodeList.add(new Tuple2<String, StringBuffer>(prototypeName, new StringBuffer(unescape)));
		return protoCodeList;
	}

//	private void loadTreplicaNd(String packageName1) {
//
		//		//System.out.println("load init: " + packName);
		//		if (!loadedPack.contains(packageName1)) {
		//			//System.out.println("load set: " + packName);
		//			loadedPack.add(packageName1);
		//			CompilerManager cm = this.compiler.getEnv().getProject().getCompilerManager();
		//
		//			Tuple2<FileError, char[]> fileList = this.compiler.getEnv().getProject().getCompilerManager()
		//					.readTextDataFileFromPackage("deterministic", packageName1);
		//			if (fileList.f2 != null) {
		//				String rawText = new String(fileList.f2);
		//				String lines[] = rawText.split("\n");
		//				for (String line : lines) {
		//					String parts[] = line.split("-");
		//					String left[] = parts[0].split(",");
		//					String right[] = parts[1].split(",");
		//					//System.out.println(left[0] + " : " + left[1]);
		//					//System.out.println(right[0] + " : " + right[1]);
		//					Tuple3<String, String, String> pLeft = new Tuple3<String, String, String>(left[0], left[1], left[2]);
		//					Tuple3<String, String, String> pRight = new Tuple3<String, String, String>(right[0], right[1], right[2]);
		//					methodsND.add(0,
		//							new Tuple2<
		//							  Tuple3<String, String, String>,
		//							  Tuple3<String, String, String>>(pLeft, pRight));
		//				}
		//			}
		//		}
//	}

	private String newMethodName(String methodName) {
		return methodName + "TreplicaAction" + this.fixName;
	}

	@Override
	public StringBuffer dsa_codeToAdd(ICompiler_dsa compiler_dsa) {
		this.compiler = compiler_dsa;
		WrEnv env = compiler.getEnv();
		IDeclaration dec = this.getMetaobjectAnnotation().getDeclaration();
		if ( !(dec instanceof WrMethodDec) ) {
			this.addError("Internal error: metaobject '" + getName() + "' should be attached to a method");
			return null;
		}
		//		mapProtoSubProto = compiler.getMapPrototypeSubtypeList();
		WrMethodDec method = (WrMethodDec) dec;
		visitorFunctionsTree(
				CheckReplace.REPLACE_NON_DETERM_CALL_BY_DETERM_CALL,
				method, this, env);
		return null;
	}

	@Override

	public void afsa_checkDeclaration(ICompiler_dsa compiler)
	//void dsa2_checkProgramUnit(ICompiler_dsa compiler)
	{
		WrEnv env = compiler.getEnv();
		IDeclaration dec = this.getMetaobjectAnnotation().getDeclaration();
		if ( !(dec instanceof WrMethodDec) ) {
			this.addError("Internal error: metaobject '" + getName() + "' should be attached to a method");
			return;
		}
		//		mapProtoSubProto = compiler.getMapPrototypeSubtypeList();
		WrMethodDec method = (WrMethodDec) dec;
		visitorFunctionsTree(CheckReplace.CHECK_NON_DETERMINISM,
				method, this, env);

	}

	/**
	   @param env
	   @param methodForMessage
	   @return
	 */
	private static boolean hasNonDeterministicAnnotation(WrEnv env,
			WrMethodDec methodForMessage) {
		/*
		 * is the method annotated as non-deterministic?
		 */
		List<WrAnnotationAt> annotList =
				methodForMessage.getAttachedMetaobjectAnnotationList(env);
		boolean foundNDannot = false;
		if ( annotList != null && annotList.size() > 0 ) {
			for ( WrAnnotationAt annot : annotList ) {
				if ( annot.getCyanMetaobject().getName()
						.equals(
								CyanMetaobjectNonDeterministic.nonDeteministicAnnotName) ) {
					foundNDannot = true;
					break;
				}
			}
		}
		return foundNDannot;
	}



//	public Map<String, Tuple2<
//	  Tuple3<String, String, String>,
//	  Tuple3<String, String, String>>> mapNDcall_to_DetermCall;


//	Vector<WrMethodDec> visited;

//	Vector<
//	Tuple2<Tuple3<String, String, String>,
//	Tuple3<String, String, String>>> methodsND;
//	WrProgram program;

//	Vector<String> loadedPack;

	String fixName = null;
	ICompiler_dsa compiler;
	private Map<String, MethodSignatureTreplica> nonDeterMessagePassing_to_NewMessagePassingMap;

}
